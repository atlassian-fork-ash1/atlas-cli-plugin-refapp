const webpack = require('webpack');

const RULE_TS = {
  test: /\.ts$/,
  use: [
    'babel-loader',
  ],
};

module.exports = {
  context: __dirname,
  entry: './src/index.ts',
  module: {
    rules: [
      RULE_TS,
    ],
  },
  output: {
    filename: 'bundle.js',
    path: `${__dirname}/dist`,
  },
  plugins: [
    new webpack.BannerPlugin({ banner: "#!/usr/bin/env node", raw: true }),
  ],
  resolve: {
    extensions: ['.ts', '.js', '.json'],
  },
  target: 'node',
};
